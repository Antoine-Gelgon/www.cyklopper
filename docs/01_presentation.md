---
title: presentation
---

<div class="image">
<img src="images-bd/presentation.svg" />
</div>

<div class="content">
<p>Cyklopper est un atelier de réparation vélo participatif visant à promouvoir l'autonomie de chacun par le partage de connaissances et la réutilisation de matériel de récupération.</p>

<p>Cyklopper c'est aussi un lieu de rencontre qui vise à promouvoir les projets de mobilités actives de chacun.e. A créer du lien dans le quartier, à vous aider à franchir le pas vers la vélonomie. </p>
</div>
